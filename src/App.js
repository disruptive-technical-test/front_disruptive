import React, { useContext } from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Header from './components/Header';
import Home from './components/Home';
import Login from './components/Login';
import Register from './components/Register';
import Content from './components/Content';
import CategoryList from './components/CategoryList';
import AddCategory from './components/AddCategoryForm';
import AddContent from './components/AddContentForm';
import ContentDetails from './components/ContentDetails';
import { AuthContext } from './context/authContext';

const App = () => {
  const { isAuthenticated } = useContext(AuthContext);
  return (
    <Router>
      <div>
        <Header isAuthenticated={isAuthenticated} />
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/login" element={<Login />} />
          <Route path="/register" element={<Register />} />
          <Route path="/contents/:categoryId" element={<Content />} />
          <Route path="/categories" element={<CategoryList />} />
          <Route path="/add-category" element={<AddCategory />} />
          <Route path="/add-content/:categoryId" element={<AddContent />} />
          <Route path="/content/:contentId" element={<ContentDetails />} />
        </Routes>
      </div>
    </Router>
  );
};

export default App;
