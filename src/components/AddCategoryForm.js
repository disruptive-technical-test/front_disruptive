import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import api from '../api';
import ErrorModal from './ErrorModal'; 

const AddCategoryForm = ({ onSuccess }) => {
  const [newCategory, setNewCategory] = useState({
    name: '',
    permissions: {
      images: false,
      videos: false,
      texts: false
    },
    coverImage: ''
  });

  const [errorMessage, setErrorMessage] = useState('');
  const [showModal, setShowModal] = useState(false);
  const navigate = useNavigate();

  const handleInputChange = (e) => {
    const { name, value, type, checked } = e.target;

    if (type === 'checkbox') {
      setNewCategory(prevCategory => ({
        ...prevCategory,
        permissions: {
          ...prevCategory.permissions,
          [name.split('.')[1]]: checked
        }
      }));
    } else {
      setNewCategory(prevCategory => ({
        ...prevCategory,
        [name]: value
      }));
    }
  };

  const handleFormSubmit = async (e) => {
    e.preventDefault();
    try {
      const token = localStorage.getItem('token');
      const headers = {
        Authorization: `Bearer ${token}`
      };
      const response = await api.post('/categories', newCategory, { headers });
      onSuccess(response.data);

      navigate('/categories');
    } catch (error) {
      if (error.response && error.response.status === 403) {
        setErrorMessage('No tienes permiso para agregar esta categoría.');
      } else {
        console.error('Error al crear nueva categoría:', error);
        setErrorMessage('Error al intentar crear la categoría. Por favor, intenta nuevamente.');
      }
      setShowModal(true);
    }
  };

  const handleCancel = () => {
    navigate('/categories');
  };

  const handleCloseModal = () => {
    setShowModal(false);
    setErrorMessage(''); 
  };

  return (
    <div className="form-container">
      <h2>Agregar Nueva Categoría</h2>
      <form onSubmit={handleFormSubmit}>
        <div className="form-group">
          <label>Nombre de la categoría</label>
          <input
            type="text"
            name="name"
            value={newCategory.name}
            onChange={handleInputChange}
            required
          />
        </div>
        <div className="form-group">
          <label>
            <input
              type="checkbox"
              name="permissions.images"
              checked={newCategory.permissions.images}
              onChange={handleInputChange}
            />
            Permitir imágenes
          </label>
        </div>
        <div className="form-group">
          <label>
            <input
              type="checkbox"
              name="permissions.videos"
              checked={newCategory.permissions.videos}
              onChange={handleInputChange}
            />
            Permitir videos
          </label>
        </div>
        <div className="form-group">
          <label>
            <input
              type="checkbox"
              name="permissions.texts"
              checked={newCategory.permissions.texts}
              onChange={handleInputChange}
            />
            Permitir textos
          </label>
        </div>
        <div className="form-group">
          <label>URL de la imagen de portada</label>
          <input
            type="text"
            name="coverImage"
            value={newCategory.coverImage}
            onChange={handleInputChange}
          />
        </div>
        <div className="form-group">
          <button type="submit">Agregar</button>
          <button type="button" onClick={handleCancel}>Cancelar</button>
        </div>
      </form>

      {/* Modal de error */}
      {showModal && <ErrorModal message={errorMessage} onClose={handleCloseModal} />}
    </div>
  );
};

export default AddCategoryForm;
