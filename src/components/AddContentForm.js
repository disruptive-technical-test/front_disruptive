import React, { useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import api from '../api';
import ErrorModal from './ErrorModal'; 

const AddContentForm = () => {
  const { categoryId } = useParams();
  const navigate = useNavigate();

  const [newContent, setNewContent] = useState({
    title: '',
    type: '',
    category: categoryId,
    fileUrl: '',
    body: '',
    imageUrl: ''
  });

  const [error, setError] = useState('');
  const [showModal, setShowModal] = useState(false);

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setNewContent({
      ...newContent,
      [name]: value
    });
  };

  const handleFormSubmit = async (e) => {
    e.preventDefault();
    try {
      const token = localStorage.getItem('token');
      const headers = {
        Authorization: `Bearer ${token}`
      };
      const response = await api.post('/content', newContent, { headers });
      console.log('Nuevo contenido creado:', response.data);
      navigate(`/contents/${categoryId}`);
    } catch (error) {
      if (error.response && error.response.status === 403) {
        setError('No tienes permiso para agregar contenido en esta categoría.');
      } else {
        console.error('Error al crear nuevo contenido:', error);
        setError('Error al intentar agregar el contenido. Por favor, intenta nuevamente más tarde.');
      }
      setShowModal(true);
    }
  };

  const handleCancel = () => {
    navigate(`/contents/${categoryId}`);
  };

  const closeModal = () => {
    setShowModal(false);
    setError('');
  };

  return (
    <div className="form-container">
      <h2>Agregar Nuevo Contenido</h2>
      <form onSubmit={handleFormSubmit}>
        <div className="form-group">
          <label>Título</label>
          <input
            type="text"
            name="title"
            value={newContent.title}
            onChange={handleInputChange}
            required
          />
        </div>
        <div className="form-group">
          <label>Tipo</label>
          <select
            name="type"
            value={newContent.type}
            onChange={handleInputChange}
            required
          >
            <option value="">Seleccione...</option>
            <option value="video">Video</option>
            <option value="text">Texto</option>
            <option value="image">Imagen</option>
          </select>
        </div>
        {newContent.type === 'video' && (
          <div className="form-group">
            <label>URL del archivo</label>
            <input
              type="text"
              name="fileUrl"
              value={newContent.fileUrl}
              onChange={handleInputChange}
            />
          </div>
        )}
        {newContent.type === 'text' && (
          <div className="form-group">
            <label>Cuerpo del texto</label>
            <textarea
              name="body"
              value={newContent.body}
              onChange={handleInputChange}
            ></textarea>
          </div>
        )}
        {newContent.type === 'image' && (
          <div className="form-group">
            <label>URL de la imagen</label>
            <input
              type="text"
              name="imageUrl"
              value={newContent.imageUrl}
              onChange={handleInputChange}
            />
          </div>
        )}
        {showModal && <ErrorModal message={error} onClose={closeModal} />}
        <div className="form-group">
          <button type="submit" className="submit-btn">Agregar</button>
          <button type="button" className="cancel-btn" onClick={handleCancel}>Cancelar</button>
        </div>
      </form>
    </div>
  );
};

export default AddContentForm;
