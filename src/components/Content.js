import React, { useState, useEffect } from 'react';
import { useNavigate, useParams, Link } from 'react-router-dom';
import api from '../api';
import ErrorModal from './ErrorModal';

const ContentList = () => {
  const [contents, setContents] = useState([]);
  const [searchTerm, setSearchTerm] = useState('');
  const navigate = useNavigate();
  const { categoryId } = useParams();

  const [error, setError] = useState('');
  const [showModal, setShowModal] = useState(false);

  useEffect(() => {
    const fetchContents = async () => {
      try {
        const token = localStorage.getItem('token');
        if (!token) {
          navigate('/login');
          return;
        }

        const headers = {
          Authorization: `Bearer ${token}`
        };

        const response = await api.get(`/content/category/${categoryId}`, { headers });
        setContents(response.data);
      } catch (error) {
        console.error('Error fetching contents:', error);
      }
    };

    fetchContents();
  }, [categoryId, navigate]);

  const handleSearchChange = (e) => {
    setSearchTerm(e.target.value);
  };

  const handleAddContent = () => {
    navigate(`/add-content/${categoryId}`);
  };

  const handleDeleteContent = async (contentId) => {
    try {
      const token = localStorage.getItem('token');
      const headers = {
        Authorization: `Bearer ${token}`
      };

      await api.delete(`/content/${contentId}`, { headers });

      const updatedContents = contents.filter(content => content._id !== contentId);
      setContents(updatedContents);
    } catch (error) {
      if (error.response && error.response.status === 403) {
        setError('No tienes permiso para eliminar este contenido.');
        setShowModal(true);
      } else {
        console.error('Error al eliminar el contenido:', error);
      }
    }
  };

  const closeModal = () => {
    setShowModal(false);
    setError('');
  };

  const filteredContents = contents.filter(content =>
    content.title.toLowerCase().includes(searchTerm.toLowerCase())
  );

  return (
    <div className="category-container">
      <h2>Contenidos</h2>
      <div className="search-bar">
        <input
          type="text"
          placeholder="Buscar contenido..."
          value={searchTerm}
          onChange={handleSearchChange}
          className="category-search"
        />
        <button className="add-button" onClick={handleAddContent}>Agregar Contenido</button>
      </div>
      <ul className="content-list">
        {filteredContents.map(content => (
          <li key={content._id} className="content-item">
            <h3>{content.title}</h3>
            <p>{content.type}</p>
            <p className="credits">Créditos: {content.credits}</p>
            <p className="category">Categoría: {content.category.name}</p>
            
            {content.type === 'video' && (
              <p className="fileUrl">URL del archivo: <a href={content.fileUrl} target="_blank" rel="noopener noreferrer">{content.fileUrl}</a></p>
            )}
            {content.type === 'text' && (
              <p className="body">{content.body}</p>
            )}
            {content.type === 'image' && (
              <div className="image-container">
                <img src={content.imageUrl} alt={content.title} />
              </div>
            )}
            <div className="content-actions">
              <Link to={`/content/${content._id}`} className="details-btn">Ver más detalles</Link>
              <button className="delete-btn" onClick={() => handleDeleteContent(content._id)}>Eliminar</button>
            </div>
          </li>
        ))}
      </ul>
      {showModal && <ErrorModal message={error} onClose={closeModal} />}
    </div>
  );
};

export default ContentList;
