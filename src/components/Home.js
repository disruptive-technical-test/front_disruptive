import React from 'react';
import { Link } from 'react-router-dom';

const Home = () => {
  return (
    <div className="home-container">
      <h1>Bienvenido a nuestro sitio web</h1>
      <p>Esta es la página de inicio. Aquí puedes encontrar información básica sobre nuestro sitio.</p>
      <p>Explora nuestras <Link to="/categories">categorías</Link> para descubrir una amplia variedad de contenido.</p>
    </div>
  );
};

export default Home;
