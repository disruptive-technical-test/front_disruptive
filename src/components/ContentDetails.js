import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import api from '../api';

const ContentDetails = () => {
  const [content, setContent] = useState(null);
  const { contentId } = useParams();

  useEffect(() => {
    const fetchContent = async () => {
      try {
        const token = localStorage.getItem('token');
        if (!token) {
          return;
        }

        const headers = {
          Authorization: `Bearer ${token}`
        };

        const response = await api.get(`/content/${contentId}`, { headers });
        setContent(response.data);
      } catch (error) {
        console.error('Error fetching content:', error);
      }
    };

    fetchContent();
  }, [contentId]);

  if (!content) {
    return <div>Cargando...</div>; 
  }

  return (
    <div className="content-details">
      <h2>{content.title}</h2>
      <p>Tipo: {content.type}</p>
      <p>Créditos: {content.credits}</p>
      <p>Categoría: {content.category.name}</p>
      
      {content.type === 'video' && (
        <p className="fileUrl">URL del archivo: <a href={content.fileUrl} target="_blank" rel="noopener noreferrer">{content.fileUrl}</a></p>
      )}
      {content.type === 'text' && (
        <p className="body">{content.body}</p>
      )}
      {content.type === 'image' && (
        <div className="image-container">
          <img src={content.imageUrl} alt={content.title} />
        </div>
      )}
      
      <button className="back-btn" onClick={() => window.history.back()}>Volver</button>
    </div>
  );
};

export default ContentDetails;
