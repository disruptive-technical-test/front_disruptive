import React, { useContext, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { AuthContext } from '../context/authContext'; 
import { jwtDecode } from "jwt-decode";

const Header = () => {
  const { authState, logout, login } = useContext(AuthContext);

  const handleLogout = () => {
    logout(); 
  };
 useEffect(() => {
    const token = localStorage.getItem('token');
    
    if (token) {
        const decoded = jwtDecode(token);
        if (decoded) {
            login(decoded.user);
        }
    }
    
  }, []);
  return (
    <header>
      <nav>
        <ul>
          <li><Link to="/">Home</Link></li>
          {authState && authState.isAuthenticated ? (
            <>
              {/* <li><Link to="/dashboard">Dashboard</Link></li> */}
              <li><Link to="/categories">Categories</Link></li>
              <li><button onClick={handleLogout}>Logout</button></li>
            </>
          ) : (
            <>
              <li><Link to="/login">Login</Link></li>
              <li><Link to="/register">Register</Link></li>
            </>
          )}
        </ul>
      </nav>
    </header>
  );
};

export default Header;
