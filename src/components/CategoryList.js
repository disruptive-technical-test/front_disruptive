import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import api from '../api';
import AddCategoryForm from './AddCategoryForm';

const CategoryList = () => {
    const [categories, setCategories] = useState([]);
    const [searchTerm, setSearchTerm] = useState('');

    useEffect(() => {
        const fetchCategories = async () => {
            try {
                const token = localStorage.getItem('token');
                if (!token) {
                    navigate('/login');
                    return;
                }

                const headers = {
                    Authorization: `Bearer ${token}`
                };

                const response = await api.get('/categories', { headers });

                setCategories(response.data);
            } catch (error) {
                console.error('Error al obtener categorías:', error);
            }
        };

        fetchCategories();
    }, []);

    const navigate = useNavigate();

    const handleCategoryClick = (categoryId) => {
        navigate(`/contents/${categoryId}`);
    };

    const handleSearchChange = (e) => {
        setSearchTerm(e.target.value);
    };

    const handleNewCategorySuccess = (newCategory) => {
        setCategories([...categories, newCategory]);
        navigate('/');
    };

    return (
        <div className="category-container">
            <h2>Categorías</h2>
            <div>
                <input
                    type="text"
                    placeholder="Buscar categoría..."
                    value={searchTerm}
                    onChange={handleSearchChange}
                    className="category-search"
                />
                <button className="add-button" onClick={() => navigate('/add-category')}>Agregar categoría</button>
            </div>
            <ul className="category-list">
                {categories.filter(category =>
                    category.name.toLowerCase().includes(searchTerm.toLowerCase())
                ).map(category => (
                    <li
                        key={category._id}
                        className="category-item"
                        onClick={() => handleCategoryClick(category._id)}
                    >
                        {category.coverImage && <img src={category.coverImage} alt={category.name} />}
                        <div>
                            <h3>{category.name}</h3>
                            <p>Permisos:</p>
                            <div className="category-permissions">
                                <span className={category.permissions.images ? 'permission-true' : 'permission-false'}>
                                    Imágenes: {category.permissions.images ? 'Sí' : 'No'}
                                </span>
                                <span className={category.permissions.videos ? 'permission-true' : 'permission-false'}>
                                    Videos: {category.permissions.videos ? 'Sí' : 'No'}
                                </span>
                                <span className={category.permissions.texts ? 'permission-true' : 'permission-false'}>
                                    Textos: {category.permissions.texts ? 'Sí' : 'No'}
                                </span>
                            </div>
                        </div>
                    </li>
                ))}
            </ul>
        </div>
    );
};

export default CategoryList;
